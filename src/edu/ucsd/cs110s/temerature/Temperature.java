/**
 * 
 */
package edu.ucsd.cs110s.temerature;

/**
 * @author kaw023
 *
 */ 
public abstract class Temperature 
{ 
 private float value;
 public Temperature(float v) 
 { 
 value = v; 
 } 
 public final float getValue() 
 { 
 return value; 
 } 
 public abstract Temperature toCelsius();  public abstract Temperature toFahrenheit();
} 

