/**
 * 
 */
package edu.ucsd.cs110s.temerature;

/**
 * @author kaw023
 *
 */
public class Celsius extends Temperature 
{ 
 public Celsius(float t) 
 { 
  super(t);
 } 
 public String toString() 
 { 
  return Float.toString(getValue()); // return the value of Value
 }
 @Override
 public Temperature toCelsius() {	 	 
	 return this;// return the Celsius itself
 }
 @Override
 public Temperature toFahrenheit() {
	 return new Fahrenheit(getValue() * 9 / 5 + 32);// return a Fahrenheit
 } 
} 
